﻿Shader "Custom/GameOfLife"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Threshold ("Threshold", Range(0, 1)) = 0.5
        _ColorMask ("ColorMask", Vector) = (1, 1, 1, 1)
        _BackgroundColor ("BG Color", Color) = (0, 0, 0, 0)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float4 _MainTex_TexelSize;
            float _Threshold;
            fixed4 _ColorMask;
            fixed4 _BackgroundColor;
            fixed4 neighbourCells[8];

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f IN) : SV_Target
            {
                neighbourCells[0] = tex2D(_MainTex, IN.uv + fixed2(-_MainTex_TexelSize.x, _MainTex_TexelSize.y));   // top left
                neighbourCells[1] = tex2D(_MainTex, IN.uv + fixed2(0, _MainTex_TexelSize.y));                       // top
                neighbourCells[2] = tex2D(_MainTex, IN.uv + fixed2(_MainTex_TexelSize.x, _MainTex_TexelSize.y));    // top right
                neighbourCells[3] = tex2D(_MainTex, IN.uv + fixed2(-_MainTex_TexelSize.x, 0));                      // left
                neighbourCells[4] = tex2D(_MainTex, IN.uv + fixed2(_MainTex_TexelSize.x, 0));                       // right
                neighbourCells[5] = tex2D(_MainTex, IN.uv + fixed2(-_MainTex_TexelSize.x, -_MainTex_TexelSize.y));  // bottom left
                neighbourCells[6] = tex2D(_MainTex, IN.uv + fixed2(0, -_MainTex_TexelSize.y));                      // bottom
                neighbourCells[7] = tex2D(_MainTex, IN.uv + fixed2(_MainTex_TexelSize.x, -_MainTex_TexelSize.y));   // bottom right
                fixed4 currentCell = tex2D(_MainTex, IN.uv);                                                        // center

                fixed3 neighbourCount = fixed3(0, 0, 0);
                for (int i = 0; i < 8; i++)
                    neighbourCount += step(_Threshold, neighbourCells[i].rgb);

                fixed3 currentlyAlive = step(_Threshold, currentCell.rgb);
                fixed3 canSurvive = neighbourCount >= 2 && neighbourCount <= 3;
                fixed3 alive = neighbourCount == 3 ? true : currentlyAlive && canSurvive;

                return lerp(_BackgroundColor, _ColorMask, fixed4(alive.rgb, 1));
            }
            ENDCG
        }
    }
}
