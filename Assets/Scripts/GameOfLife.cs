﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine.UI;

public class GameOfLife : MonoBehaviour
{
    public enum ConfigurationOption
    {
        TextFile,
        Texture,
        Random
    }

    private enum TokenType
    {
        Blank,
        Occupied,
        EndOfLine
    }

    [Serializable]
    public class GameConfiguration
    {
        public ConfigurationOption configurationMethod;
        public TextAsset startStateTextAsset;
        public Texture2D startStateTexture;
        public bool useRandomSeed = false;
        public int randomSeed;
        public Vector2 scaling = Vector2.one;
        public Vector2 offset;
    }

    public Material material;
    public Color occupiedColor = Color.white;
    [Range(1, 1000)]
    public int targetFrameRate = 20;
    [Range(1, 50)]
    public int stepsPerFrame = 1;
    public GameConfiguration[] configs;

    [Space(10)]
    public TextMeshProUGUI generationText;
    public Slider stepSlider;
    public Slider fpsSlider;

    private RenderTexture rt1;
    private RenderTexture rt2;
    private int rtIndex;
    private int generation = 1;
    private bool paused;

    private const string OccupiedString = "o";
    private const string EolString = "$";

    private void Start()
    {
        Setup(this.configs[0]);
    }

    private void Setup(GameConfiguration config)
    {
        var width = Mathf.RoundToInt(config.scaling.x * Screen.width);
        var height = Mathf.RoundToInt(config.scaling.y * Screen.height);
        var tex = new Texture2D(width, height) { filterMode = FilterMode.Point };
        Screen.SetResolution(width, height, true);

        if (config.useRandomSeed)
            Random.InitState(config.randomSeed);

        switch (config.configurationMethod)
        {
            case ConfigurationOption.TextFile:
                ParseTextFile(config, tex);
                break;

            case ConfigurationOption.Texture:
                tex.SetPixels(0, 0, tex.width, tex.height, new Color[tex.width * tex.height]);
                var texWidth = Mathf.Min(config.startStateTexture.width, tex.width);
                var texHeight = Mathf.Min(config.startStateTexture.height, tex.height);
                var texY = Mathf.Max(0, tex.height - config.startStateTexture.height);
                tex.SetPixels(0, texY, texWidth, texHeight, config.startStateTexture.GetPixels());
                break;

            case ConfigurationOption.Random:
                for (var y = 0; y < tex.height; y++)
                    for (var x = 0; x < tex.width; x++)
                        tex.SetPixel(x, y, new Color(Random.Range(0, 2), Random.Range(0, 2), Random.Range(0, 2), 1f));
                break;

            default:
                throw new ArgumentOutOfRangeException();
        }

        tex.Apply();

        this.rt1 = new RenderTexture(width, height, 0);
        this.rt2 = new RenderTexture(width, height, 0);
        this.rt1.filterMode = FilterMode.Point;
        this.rt2.filterMode = FilterMode.Point;
        this.rtIndex = 0;
        this.generation = 2;
        this.paused = false;
        Graphics.Blit(tex, this.rt1, this.material);
    }

    private void ParseTextFile(GameConfiguration config, Texture2D tex)
    {
        tex.SetPixels(0, 0, tex.width, tex.height, new Color[tex.width * tex.height]);
        var rleText = config.startStateTextAsset.text;
        rleText = rleText.Replace("\n", "");
        rleText = rleText.Replace("\r", "");
        rleText = rleText.Replace("!", "");

        var startX = (tex.width / 2) + (int) config.offset.x;
        var y = (tex.height / 2) - (int) config.offset.y;
        var x = startX;

        var re = new Regex(@"(\d+)?([bo$])");
        var matches = re.Matches(rleText);
        foreach (Match match in matches)
        {
            GroupCollection groups = match.Groups;
            var count = Convert.ToInt32(groups[1].Value == string.Empty ? "1" : groups[1].Value);

            var tokenType = TokenType.Blank;
            switch (groups[2].Value)
            {
                case OccupiedString:
                    tokenType = TokenType.Occupied;
                    break;
                case EolString:
                    tokenType = TokenType.EndOfLine;
                    break;
            }

            switch (tokenType)
            {
                case TokenType.EndOfLine:
                    x = startX;
                    y -= count;
                    break;
                case TokenType.Blank:
                    for (var i = 0; i < count; i++)
                    {
                        tex.SetPixel(x, y, Color.black);
                        x++;
                    }
                    break;
                case TokenType.Occupied:
                    for (var i = 0; i < count; i++)
                    {
                        tex.SetPixel(x, y, this.occupiedColor);
                        x++;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    [ContextMenu("Pause")]
    public void Pause()
    {
        this.paused = true;
    }

    [ContextMenu("Resume")]
    public void Resume()
    {
        this.paused = false;
    }

    public void LoadPattern(int index)
    {
        Setup(this.configs[index]);
    }

    private void Update()
    {
        this.generationText.text = "Generation: " + this.generation;
        this.stepsPerFrame = (int) this.stepSlider.value;
        this.targetFrameRate = (int) this.fpsSlider.value;
        Application.targetFrameRate = this.targetFrameRate;
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        if (this.paused)
            return;

        for (var i = 0; i < this.stepsPerFrame; i++)
        {
            var rtSrc = this.rtIndex == 0 ? this.rt1 : this.rt2;
            var rtDst = this.rtIndex == 1 ? this.rt1 : this.rt2;
            Graphics.Blit(rtSrc, rtDst, this.material);
            this.rtIndex = (this.rtIndex + 1) % 2;
            this.generation++;

            if (i != this.stepsPerFrame - 1)
                continue;

            Graphics.Blit(rtDst, dst, this.material);
            this.generation++;
        }
    }
}
